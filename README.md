# Set of basic roles to install Apache CloudStack with some example ansible plays to use them.
# License
Apache License

## Caveats
Only works on Ubuntu at this time.

Basic installs with database and CloudStack management on the same server.

Uses basic NFS primary and Secondary storage, but easy to skip those roles to use something else.

No examples yet of setting up the Pods, Cluster, Networking, etc.

Assumes KVM.

Likely bugs in how CloudStack is being installed outside of basic assumption.

## The Roles
**cloudstack** - Basic role for commong requires of install CloudStack

**cloudstack-compute** - Packages and config for a compute node

**cloudstack-manager** - Packages and config for a management node and loading systemvm template

**cloudstack-cli** - Package and config for pip cloudmonkey or pip cs installs. **TODO** use newer cloudmonkey written in go

**cloudstack-source** - Packages and building deb packages from source

**cloudstack-storage** - NFS Primary and Secondary storage setup

**cloudstack-storage-mount** - Mounting of the above storage

## TODOS
* Support more than just Ubuntu
* Better apt key installation
* More robust example plays, particular to install a Region, Zone, Pod, and Cluster
* Roles to configure other storage
* Plus various other improvements